// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Gun.h"
#include "Components/CapsuleComponent.h"
#include "SimpleShooterGameModeBase.h"
#include "ShooterPlayerController.h"
#include "UsableItem.h"


// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;

	GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
	
	int i = 0;
	for (TSubclassOf<AGun> GunClass : GunClassArray)
	{
		AGun* Gun = GetWorld()->SpawnActor<AGun>(GunClass);
		Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
		Gun->SetOwner(this);
		Gun->SetHidden(i != ActiveGunIndex);
		InstancedGuns.Add(Gun);

		i++;
	}
}

bool AShooterCharacter::IsDead() const
{
	return Health <= 0;
}

float AShooterCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}

AGun* AShooterCharacter::GetCurrentGun() const
{
	return InstancedGuns[ActiveGunIndex];
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("SwapGun"), this, &AShooterCharacter::SwapGun);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterCharacter::LookRightRate);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AShooterCharacter::Fire);
	PlayerInputComponent->BindAction(TEXT("Use"), IE_Pressed, this, &AShooterCharacter::Use);
}

float AShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const &DamageEvent, AController *EventInstigator, AActor *DamageCauser) 
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	DamageToApply = FMath::Min(Health, DamageToApply);
	Health -= DamageToApply;
	UE_LOG(LogTemp, Warning, TEXT("Health left: %f"), Health);

	if (IsDead())
	{
		ASimpleShooterGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASimpleShooterGameModeBase>();
		if (GameMode)
		{
			GameMode->PawnKilled(this);
		}
		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	return DamageToApply;
}

void AShooterCharacter::MoveForward(float axisValue) 
{
	AddMovementInput(GetActorForwardVector() * axisValue);
}

void AShooterCharacter::MoveRight(float axisValue) 
{
	AddMovementInput(GetActorRightVector() * axisValue);
}

void AShooterCharacter::LookUpRate(float axisValue) 
{
	AddControllerPitchInput(axisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::LookRightRate(float axisValue) 
{
	AddControllerYawInput(axisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::SwapGun(float axisValue) 
{
	if (axisValue == 0.f) return;
	
	int NewActiveGunIdx = ActiveGunIndex + int(axisValue);

	// Set new index in array range
	NewActiveGunIdx = (NewActiveGunIdx % GunClassArray.Num() + GunClassArray.Num()) % GunClassArray.Num();
	
	InstancedGuns[NewActiveGunIdx]->SetActorHiddenInGame(false);
	InstancedGuns[ActiveGunIndex]->SetActorHiddenInGame(true);
	ActiveGunIndex = NewActiveGunIdx;
}

void AShooterCharacter::Use() 
{
	AShooterPlayerController* ShooterPlayerController = Cast<AShooterPlayerController>(GetController());
	if (!ShooterPlayerController) return;

	AUsableItem* FocusedItem = ShooterPlayerController->GetFocusedItem();
	if (!FocusedItem) return;

	switch(FocusedItem->GetItemType())
	{
		case UsableItemType::HEAL:
			Health = FMath::Min(Health + FocusedItem->GetItemActionAmount(), MaxHealth);
			break;
		
		case UsableItemType::AMMO:
			InstancedGuns[ActiveGunIndex]->AddAMMO(FocusedItem->GetItemActionAmount());
			break;

		default:
			UE_LOG(LogTemp, Error, TEXT("No type detected for item '%s'"), *FocusedItem->GetItemName());
			return;
	}

	FocusedItem->PlayGrabSound();

	if (!FocusedItem->Destroy())
	{
		UE_LOG(LogTemp, Error, TEXT("Could not destroy item '%s'"), *FocusedItem->GetItemName());
		return;
	}
}

void AShooterCharacter::Fire() 
{
	if (ActiveGunIndex >= InstancedGuns.Num())
	{
		UE_LOG(LogTemp, Error, TEXT("Active Gun Index out of bounds!"));
		return;
	}
	
	InstancedGuns[ActiveGunIndex]->PullTrigger();
}