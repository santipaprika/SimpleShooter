// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ShooterPlayerController.generated.h"

class AUsableItem;

UCLASS()
class SIMPLESHOOTER_API AShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void GameHasEnded(class AActor* EndGameFocus = nullptr, bool bIsWinner = false) override;

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure)
	AUsableItem* GetFocusedItem() const;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> LoseScreenClass;
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> WinScreenClass;
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> HUDClass;
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> UsableItemHUDClass;
	UPROPERTY(EditAnywhere)
	float ItemHUDVerticalOffset = 100;
	UPROPERTY(EditAnywhere)
	float ItemHUDMaxDistance = 1000;

	UPROPERTY(EditAnywhere)
	float RestartDelay = 5;

	FTimerHandle RestartTimer;

	UPROPERTY()
	UUserWidget* HUD = nullptr;
	UPROPERTY()
	UUserWidget* UsableItemHUD = nullptr;

	AUsableItem* FocusedItem = nullptr;

	void ManageItemHUD();
	void UpdateItemHUDPosition();
	void ShowWidgetOnViewport(UUserWidget* Widget, bool bShow);

};
