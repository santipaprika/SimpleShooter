// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UsableItem.generated.h"

UENUM(BlueprintType)
enum UsableItemType {
	HEAL UMETA(DisplayName="Heal"), 
	AMMO UMETA(DisplayName="AMMO")
};

UCLASS()
class SIMPLESHOOTER_API AUsableItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUsableItem();

	UFUNCTION(BlueprintPure)
	FString GetItemName() const;
	UsableItemType GetItemType() const;
	float GetItemActionAmount() const;
	void PlayGrabSound() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
	USceneComponent *Root;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent *Mesh;

	UPROPERTY(EditAnywhere)
	USoundBase *GrabSound;

	UPROPERTY(EditAnywhere)
	FString Name = TEXT("Unidentified item");

	UPROPERTY(EditAnywhere)
	TEnumAsByte<UsableItemType> ItemType;
	
	UPROPERTY(EditAnywhere)
	float Amount = 10;

};
