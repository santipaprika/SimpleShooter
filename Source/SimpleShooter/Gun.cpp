// Fill out your copyright notice in the Description page of Project Settings.

#include "Gun.h"
#include "Kismet/GameplayStatics.h"

#define OUT

// Sets default values
AGun::AGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);
}

void AGun::PullTrigger()
{
	if (GetWorldTimerManager().GetTimerElapsed(TimerHandle) > 0) return;

	if (AMMO == 0)
	{
		UGameplayStatics::SpawnSoundAttached(EmptyGunSound, Mesh, TEXT("MuzzleFlashSocket"));
		return;
	}

	AMMO--;

	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));

	FHitResult Hit;
	FVector ShotDirection;
	bool bSuccess = GunTrace(Hit, ShotDirection);

	if (bSuccess)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.Location, ShotDirection.Rotation());
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ImpactSound, Hit.Location, ShotDirection.Rotation());


		AActor *ActorHit = Hit.GetActor();
		if (ActorHit)
		{
			FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);

			AController* OwnerController = GetOwnerController();
			ActorHit->TakeDamage(Damage, DamageEvent, OwnerController, this);
		}
	}

	GetWorldTimerManager().SetTimer(TimerHandle, FireRate, false);
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();

	TimerHandle = GetWorldTimerManager().GenerateHandle(0);
	AMMO = MaxAMMO;
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float AGun::GetRemainingFireReloadTime() const
{
	return FMath::Clamp(GetWorldTimerManager().GetTimerRemaining(TimerHandle) / FireRate, 0.f, 1.f);
}

int AGun::GetAMMO() const
{
	return AMMO;	
}

void AGun::AddAMMO(int AddedAMMO)
{
	AMMO = FMath::Min(AMMO + AddedAMMO, MaxAMMO);
}

int AGun::GetMaxAMMO() const
{
	return MaxAMMO;
}

FString AGun::GetName() const
{
	return Name;
}

bool AGun::GunTrace(FHitResult &Hit, FVector &ShotDirection) 
{
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr)
		return false;

	FVector Location;
	FRotator Rotation;

	OwnerController->GetPlayerViewPoint(OUT Location, OUT Rotation);
	ShotDirection = -Rotation.Vector();

	FVector End = Location + Rotation.Vector() * MaxRange;

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());
	return GetWorld()->LineTraceSingleByChannel(OUT Hit, Location, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
}

AController* AGun::GetOwnerController() const
{
	APawn *OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr)
		return nullptr;
	return OwnerPawn->GetController();
}
