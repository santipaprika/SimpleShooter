// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterPlayerController.h"
#include "TimerManager.h"
#include "Blueprint/UserWidget.h"
#include "UsableItem.h"

void AShooterPlayerController::GameHasEnded(class AActor *EndGameFocus, bool bIsWinner)
{
    Super::GameHasEnded(EndGameFocus, bIsWinner);

    HUD->RemoveFromViewport();
    
    if (bIsWinner)
    {
        UUserWidget *WinScreen = CreateWidget(this, WinScreenClass);
        if (WinScreen)
        {
            WinScreen->AddToViewport();
        }
    }
    else
    {
        UUserWidget *LoseScreen = CreateWidget(this, LoseScreenClass);
        if (LoseScreen)
        {
            LoseScreen->AddToViewport();
        }
    }

    GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, RestartDelay);
}

void AShooterPlayerController::Tick(float DeltaTime) 
{
    Super::Tick(DeltaTime);

    ManageItemHUD();
}

AUsableItem* AShooterPlayerController::GetFocusedItem() const
{
    return FocusedItem;
}

void AShooterPlayerController::BeginPlay()
{
    Super::BeginPlay();

    HUD = CreateWidget(this, HUDClass);
    if (HUD)
    {
        HUD->AddToViewport();
    }

    UsableItemHUD = CreateWidget(this, UsableItemHUDClass);
    
    if (!UsableItemHUD)
    {
        UE_LOG(LogTemp, Error, TEXT("No UsableItemHUD found on ShooterPlayerController."));
        SetPause(true);
    }
}

void AShooterPlayerController::ManageItemHUD() 
{
    FVector Location;
	FRotator Rotation;

	GetPlayerViewPoint(OUT Location, OUT Rotation);
	FVector ShotDirection = -Rotation.Vector();
	FVector End = Location + Rotation.Vector() * 10000.f;

    FHitResult Hit;

    if (GetWorld()->LineTraceSingleByObjectType(Hit, Location, End, FCollisionObjectQueryParams(ECollisionChannel::ECC_GameTraceChannel2))) 
    {
        if (Hit.Distance > ItemHUDMaxDistance)
        {
            ShowWidgetOnViewport(UsableItemHUD, false);
            FocusedItem = nullptr;
            return;
        }

        FocusedItem = Cast<AUsableItem>(Hit.GetActor());
        if (FocusedItem)
        {
            ShowWidgetOnViewport(UsableItemHUD, true);
            UpdateItemHUDPosition();
        }
    }
    else
    {
        ShowWidgetOnViewport(UsableItemHUD, false);
        FocusedItem = nullptr;
    }
}

void AShooterPlayerController::UpdateItemHUDPosition() 
{
    FVector2D ScreenLocation;
    APlayerController::ProjectWorldLocationToScreen(FocusedItem->GetActorLocation() + FVector(0.f, 0.f, ItemHUDVerticalOffset), ScreenLocation);
    UsableItemHUD->SetPositionInViewport(ScreenLocation);
}

void AShooterPlayerController::ShowWidgetOnViewport(UUserWidget* Widget, bool bShow) 
{
    bool performAction = Widget->IsInViewport() != bShow; 

    if (performAction)
    {
        bShow ? Widget->AddToViewport() : Widget->RemoveFromViewport();
    }
}
