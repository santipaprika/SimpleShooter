// Fill out your copyright notice in the Description page of Project Settings.


#include "UsableItem.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AUsableItem::AUsableItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);
}

FString AUsableItem::GetItemName() const
{
	return Name;
}

UsableItemType AUsableItem::GetItemType() const
{
	return ItemType;
}

float AUsableItem::GetItemActionAmount() const
{
	return Amount;
}

void AUsableItem::PlayGrabSound() const
{
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), GrabSound, GetActorLocation());
}

// Called when the game starts or when spawned
void AUsableItem::BeginPlay()
{
	Super::BeginPlay();
}

