# SimpleShooter
Learning Project for the Unreal Engine course by GameDev.tv on Udemy.  
The goal of this game is to eliminate all enemies without dying.  
You can download the **playable version [here](https://mega.nz/file/NuQ1nKKB#hdDrr84jI2CP5IM6I2PgAId-FtNWRlbTspdNfbb2nXw)** (for Windows x64).

## Demo
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/8Kq9ocG6Q5I/0.jpg)](https://www.youtube.com/watch?v=8Kq9ocG6Q5I)

## About the project
Most assets have been provided by the course instructors, the Epic Games' Marketplace or Freesound.  
  
Game behavior has been coded following the course videos and instructors tips. This part includes: 
- C++ classes (Pawns, actors, controllers...)
- Character movement
- Animation programming
- Animation state machines
- UE blueprints
- Line tracing
- Particles FX and SFX binding
- HP and damage system
- AI (C++ / Behavior Trees) 
 
## Additional (custom) features
These are additional features that I have included on my own.

- Scene setup.
- Gun-dependant fire rate.
- Fire rate visualization.
- Usable Items: Those can be used only if the player is close enough and pointing to them.
    - Medical Kit: Heals the player for a given ammount.
    - AMMO Packs: Add an ammount of AMMO to the current active gun's load.
- Weapon switching.
